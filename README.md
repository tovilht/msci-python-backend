<!-- TABLE OF CONTENTS -->

## Table of Contents

<details open="open">
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#folder-structure">Folder Structure</a></li>
    <li><a href="#code-explanation">Code Explanation</a>
    <ul>
        <li><a href="#file-conversion">FileConversion.py</a></li>
        <li><a href="#predict">Predict.py</a></li>
        <li><a href="#app">App.py</a></li>
      </ul>
    </li>
   
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

The project is built for the classifcation of various types of DNAs.

this Python backend will serve the following purposes:

1. it allows the upload of .abf and .dat files
2. it converts the .abf files into .csv files, and will perform baseline corrections and fast fourier transform.
3. it allows the choice of different models uploaded by the users
4. it allows the prediction of the data
5. it allows users to download the result as a .png file

### Built With

The backend is build with Python, while the following packages were used:

1. Flask
2. Scipy
3. Pandas
4. Numpy
5. Shutil
6. Pyabf
7. Os
8. Tqdm
9. Joblib
10. Matplotlib
11. Tensorflow
12. werkzeug.utils
13. datetime
14. threading

<!-- GETTING STARTED -->

## Getting Started

Please follow the instructions below for installation.

### Prerequisites

1. python 3.8.5 or above

### Installation

1. `pip install` all of the packages specified above

<!-- USAGE EXAMPLES -->

<!-- Folder Structure -->

## Folder Structure

├── **data** (where all the .abf files uploaded will be stored)

├── **env** (flask environment, do not touch)

├── **img** (where all the result images are stored)

├── **model** (where all .dat models uploaded will be stored)

├── **script** (where all scripts all stored)

└── **README.md**

## Code Explanation

### Introduction

This section will explain the files in the `script` file.

### File Conversion

- `baseline_algo`: this method is the algorithm for baseline corection. The mathematics behind the algorithm can be found at Asymmetric Least Squares Smoothing by P. Eilers and H. Boelens.

- `baseline_correction`: this method splits the data into segments with each segments having 10,000 data points. it uses the `baseline_algo` method to perform baseline correction then stored in an array. the function finally return the array with all baseline corrected data as the output. Note that the `tqdm` function is just for python to show its progress.

- `current`: this method will read the .abf file and sweep through to read the current. since the current has negative value, it also inverts the current so that the peaks have positive values.

- `to_csv`: this method uses the Pandas library and stores the data into a .csv file. The baseline correction is very time consuming, in order to run the prediction on various model efficiently, it is better to save the baseline corrected data and reuse for prediction. the function will store files in the `./data` file, and the file will be renamed based on the timestamp. The actual time will be passed from the frontend as string.

### Predict

- `preprocess`: this method performs the normalisation and fast fourier transform operations. For normalisation, `tf.keras.utils.normalize()` was used. For FFT operations, the first data point has very high value and hence was omitted. The FFTed data was further normalisaed so that all of the segments are of the same scale. the data was appended in an array and returned as output.

- `predict`: the model was looded using `joblib`, with `prob` and `result` returned. The `prob` is the probability of each outcome, while the `result` only concerns with the predicted outcome.

- `plot`: this method performs the plotting for the `barcode` graphs, which marks the prediction for each section with different colors. since only five different DNA were used in the training, the `color_scheme` was defined in the constuctor with

```py
{1: 'red', 5: 'orange', 10: 'yellow', 20: 'green', 485: 'blue'}
```

The above color_scheme can be more dynamic by allowing the users to input the dependent variables and the corresponding color according to the user's preference.

However, the current system is not dynamic and is fixed to five different types of DNA. However, the system can alos perform an analysis with any combination of the five DNAs, e.g. a mix of four DNAs in the above object.

- `numbers_only_prediction`: This method returns the probability of the prediction, which allows the users to download the raw data.

## App

### Introduction

This file specifies the APIs for frontend portal to GET and POST data.

### Do not touch

```py
app = Flask(__name__)
cors = CORS(app)
app.secret_key = "super secret key"
```

The above code are basic configurations such that cross-origin access is accessible.

### Get Data

- `/get_data/<extension>`: this GET method allows the retrival of .csv and .abf files. specify the `extension` variable in the link to get the files.

- `/get_model/<extension>`: this GET method specifically retrives .dat models.

- `/get_result`: this GET method retrives all the images from the `images` file

- `/get_img/<file_name>`: this GET method allows download of the image file

### File Upload

- `/file_upload/<destination>`: this POST method allows the upload of both the .abf and .dat files. the destination has to be specified in the api so that the server will know where it needs to store the data.

### File Conversion

- `/file_conversion/<file>`: this GET method allows the conversion of the .abf file into .csv file with baseline corrections. Threading is applied to allow multiple file conversion at the same time

### Prediction

- `/prediction/<file>/<model>`: this GET request allow the user to specify the data to be predicted and the model being used in the prediction. Threading is also applied in this API since there may be multiple predictions at the same time. Also, runtime error will occur if no threading was applied since it takes a while to the prediction to complete.
