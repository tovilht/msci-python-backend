# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 09:43:12 2021

@author: Tovi
"""
from scipy import sparse
from scipy.sparse.linalg import spsolve
from tqdm import tqdm
import pandas as pd
import numpy as np
import shutil
import pyabf
import os


class FileConversion:

    def __init__(self, directory, file_name, destination):
        current = self.current(directory, file_name)
        array = self.baseline_correction(current)
        self.to_csv(array, file_name, destination)

    def baseline_algo(self, y, lam, p, niter=10):
        L = len(y)
        D = sparse.diags([1, -2, 1], [0, -1, -2], shape=(L, L-2))
        D = lam * D.dot(D.transpose())
        w = np.ones(L)
        W = sparse.spdiags(w, 0, L, L)
        for i in range(niter):
            W.setdiag(w)
            Z = W + D
            z = spsolve(Z, w*y)
            w = p * (y > z) + (1-p) * (y < z)
        return z

    def baseline_correction(self, current):
        split = 10000
        array = []
        for n in tqdm(range(0, len(current))):
            if n % split == 0:
                segment = current[n:n+split]
                baseline = self.baseline_algo(segment, 1000, 0.05)
                new_segment = segment - baseline
                array.append(new_segment)
        return array

    def current(self, directory, file_name):
        file_name = file_name + '.abf'
        abf = pyabf.ABF(os.path.join(directory, file_name))
        abf.setSweep(0)
        current = abf.sweepY
        current = -current
        return current

    def to_csv(self, array, file_name, destination):
        file_name = file_name + '.csv'
        df = pd.DataFrame(data=array)
        df.to_csv(file_name, index=False)
        from_location = './' + file_name
        to_location = os.path.join(destination, file_name)
        shutil.move(from_location, to_location)
