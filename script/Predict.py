# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 15:11:55 2021

@author: Tovi
"""
import os
import pandas as pd
import numpy as np
import joblib
import tensorflow as tf
import matplotlib.pyplot as plt
from scipy.fft import fft

plt.rcParams['agg.path.chunksize'] = 10000


class Predict:

    def __init__(self, data_path, model_path):
        self.colour_scheme = {1: 'red', 5: 'orange',
                              10: 'yellow', 20: 'green', 485: 'blue'}
        self.data_path = data_path
        self.model_path = model_path

    def preprocess(self, data_path):
        csv = pd.read_csv(data_path)
        csv = tf.keras.utils.normalize(csv)
        data = []
        for each in csv.values:
            yf = abs(fft(each))[1:]
            yf = yf/max(yf)
            data.append(yf)
        data = np.array(data)
        return data

    def predict(self, model_path, data):
        model = joblib.load(model_path)
        prob = model.predict_proba(data)
        result = model.predict(data)
        return prob, result

    def plot(self, image_name):
        file = pd.read_csv(self.data_path)
        original = np.concatenate(file.values)
        data = self.preprocess(self.data_path)
        prob, result = self.predict(self.model_path, data)

        print(prob)

        plt.figure(figsize=(100, 10))
        plt.plot(original)
        for i in range(0, len(result)):
            start = i*10**4
            end = start + 10**4
            plt.axvspan(start, end, color=self.colour_scheme[[
                        *self.colour_scheme][result[i]]], alpha=0.5, lw=0)
        plt.savefig(os.path.join('./img', image_name))
        return original, result

    def numbers_only_prediction(self):
        file = pd.read_csv(self.data_path)
        original = np.concatenate(file.values)
        data = self.preprocess(self.data_path)
        prob, result = self.predict(self.model_path, data)
        return prob
