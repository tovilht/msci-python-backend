# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 17:51:16 2021

@author: Tovi
"""

from FileConversion import FileConversion
from Predict import Predict 

#file_conversion: loc of .abf, file_name of .abf, loc of .csv
#file_conversion = FileConversion('../data', 'P11_100pMmix_300mV_0002', '../data')

# predict: loc of csv, loc of model
predict = Predict('../data/P11_100pMmix_300mV_0002.csv', '../model/CSV_XGB_all_data.dat')

# image name
predict.plot('final testing again')
