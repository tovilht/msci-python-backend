from flask import Flask, flash, jsonify, request, redirect, url_for, send_from_directory, send_file
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename
from datetime import datetime
import threading
import os

from script.FileConversion import FileConversion
from script.Predict import Predict

app = Flask(__name__)
cors = CORS(app)
app.secret_key = "super secret key"

# ROUTE START

# ACTIONS START


@app.route('/file_conversion/<file>', methods=['GET'])
def file_conversion(file):

    file_name = file.split('.abf')[0]
    print(file_name)

    def task(**kwargs):
        FileConversion('./data', file_name, './data')
    thread = threading.Thread(target=task)
    thread.start()
    return {"message": "Accepted"}, 202


@app.route('/prediction/<file>/<model>', methods=['GET'])
def prediction(file, model):
    path = os.path.join('./data', file)

    now = datetime.now()
    file_name = int(datetime.timestamp(now))

    def task(**kwargs):
        predict = Predict(path, './model/'+model)
        predict.plot(str(file_name))
    thread = threading.Thread(target=task)
    thread.start()
    return {"file_name": str(file_name)}, 202


# ACTIONS END

# GET DATA START


@app.route('/get_data/<extension>', methods=['GET'])
def get_data(extension):
    output = []
    paths = os.listdir('./data')
    for path in paths:
        if path.endswith('.' + extension):
            output.append({'file_name': path, 'selected': False})
    return jsonify(output)


@app.route('/get_model/<extension>', methods=['GET'])
def get_model(extension):
    output = []
    paths = os.listdir('./model')
    for path in paths:
        if path.endswith('.' + extension):
            output.append({'file_name': path, 'selected': False})
    return jsonify(output)


@app.route('/get_result', methods=['GET'])
def get_result():
    output = []
    paths = os.listdir('./img')
    for path in paths:
        output.append({'file_name': path})
    return jsonify(output)


@app.route('/get_img/<file_name>', methods=['GET'])
def get_img(file_name):
    path = os.path.join('./img', file_name)
    return send_file(path,
                     attachment_filename=file_name)


# GET DATA END


# PERFORMANCE END

# FILE UPLOAD START
UPLOAD_FOLDER = './data'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/file_upload/<destination>', methods=['POST'])
@cross_origin()
def upload_file(destination):
    if 'file' not in request.files:
        flash('No file part')
        return 'No Files'

    file = request.files['file']

    if file.filename == '':
        flash('No selected file')
        return 'No Selected File'

    if file:
        filename = secure_filename(file.filename)
        extension = os.path.splitext(filename)[1]
        path = os.path.join(destination, filename)
        file.save(path)

        new_name = os.path.join(
            destination, request.values['file_name'] + str(extension))
        os.rename(path, new_name)
        return 'Upload Successful'


# ROUTE END
if __name__ == '__main__':
    app.run(debug=True)
